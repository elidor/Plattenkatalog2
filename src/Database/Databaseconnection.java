package Database;

import Objekte.Platte;
import org.h2.jdbcx.JdbcDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Databaseconnection {
    JdbcDataSource ds = new JdbcDataSource();


    private Platte getPlatte(int pID) {
        Connection conn = null;
        try {
            conn = ds.getConnection();
            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT TOP 1000 * FROM PLATTEN WHERE ID = " + pID);
            while (rs.next()) {
                Platte p = new Platte();
                p.setId(rs.getInt("ID"));
                p.setName(rs.getString("ALBUM"));
                p.setInterpret(rs.getString("INTERPRET"));
                p.setYear(rs.getInt("YEAR_ALB"));
                p.setGenre(rs.getString("GENRE"));
                p.setCond(rs.getString("CONDITION"));
                p.setOwner(rs.getString("OWNER"));
                p.setLocation(rs.getString("LOCATION"));
                p.setPurchase(rs.getInt("PURCHASE"));
                p.setForsell(rs.getInt("FORSELL"));
                p.setSongs(rs.getString("SONGS"));
                p.setKatnr(rs.getString("KATNR"));
                p.setOther(rs.getString("OTHER"));

                return p;
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }

    public Platte[] search(String pSuchbegriff) { //TODO: Mehr Suchfunktion als nur das Album hinzufügen
        Connection conn = null;
        ArrayList<Platte> al = new ArrayList<Platte>();
        try {
            conn = ds.getConnection();
            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT TOP 1000 * FROM PLATTEN WHERE "
                    + "UPPER(ALBUM) LIKE UPPER('%" + pSuchbegriff + "%') "
                    + "OR UPPER(INTERPRET) LIKE UPPER('%" + pSuchbegriff + "%') "
                    + "OR UPPER(OTHER) LIKE UPPER('%" + pSuchbegriff + "%') "
                    + "OR UPPER(LOCATION) LIKE UPPER('%" + pSuchbegriff + "%') "
                    + "ORDER BY INTERPRET");
            while (rs.next()) {
                al.add(getPlatte(rs.getInt("ID")));
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }

        Platte[] p = new Platte[al.size()];
        for (int i = 0; i < al.size(); i++) {
            p[i] = al.get(i);
        }
        return p;
    }

    public void deleteEntry(Platte p) {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM PLATTEN WHERE ID=" + p.getId());
        updateTable(sb.toString());
    }

    /**
     * Wenn Platte eine ID hat, wird der Eintrag aktualisiert, wenn nicht, wird ein neuer erstellt.
     *
     * @param p
     */
    public void refEntry(Platte p) {
        StringBuilder sb = new StringBuilder();
        if (p.hasId()) {


            sb.append("UPDATE PLATTEN SET ");
            sb.append("ALBUM='" + p.getName() + "'");
            sb.append(",INTERPRET='" + p.getInterpret() + "'");
            sb.append(",YEAR_ALB=" + p.getYear() + "");
            if (p.getGenre() != null)
                sb.append(",GENRE='" + p.getGenre() + "'");
            if (p.getCond() != null)
                sb.append(",CONDITION='" + p.getCond() + "'");
            if (p.getOwner() != null)
                sb.append(",OWNER='" + p.getOwner() + "'");
            if (p.getLocation() != null)
                sb.append(",LOCATION='" + p.getLocation() + "'");

            sb.append(",PURCHASE=" + p.getPurchase() + "");
            sb.append(",FORSELL=" + p.getForsell() + "");
            if (p.getSongs() != null)
                sb.append(",SONGS='" + p.getSongs() + "'");
            if (p.getKatnr() != null)
                sb.append(",KATNR='" + p.getKatnr() + "'");
            if (p.getOther() != null)
                sb.append(",OTHER='" + p.getOther() + "'");
            sb.append(" WHERE ID=" + p.getId());
        } else {
            sb.append("INSERT INTO PLATTEN (ALBUM, INTERPRET, YEAR_ALB, GENRE, CONDITION, OWNER," +
                    " LOCATION, PURCHASE, FORSELL, SONGS, KATNR, OTHER) VALUES(");
            sb.append("'" + p.getName() + "'");
            sb.append(",'" + p.getInterpret() + "'");
            sb.append("," + p.getYear());
            sb.append(",'" + p.getGenre() + "'");
            sb.append(",'" + p.getCond() + "'");
            sb.append(",'" + p.getOwner() + "'");
            sb.append(",'" + p.getLocation() + "'");
            sb.append("," + p.getPurchase());
            sb.append("," + p.getForsell());
            sb.append(",'" + p.getSongs() + "'");
            sb.append(",'" + p.getKatnr() + "'");
            sb.append(",'" + p.getOther() + "');");
        }
        updateTable(sb.toString());
    }

    private void updateTable(String sql) {
        Statement stmt = null;
        Connection connection = null;

        try {
            connection = ds.getConnection();
            stmt = connection.createStatement();
            stmt.executeUpdate(sql);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Databaseconnection() throws Exception {
        Connection conn = null;
        try {

            Class.forName("org.h2.Driver");

            ds.setURL("jdbc:h2:file:/PlattenDat");
            conn = ds.getConnection();

            Statement stmt = conn.createStatement();

            /*ResultSet rs = stmt.executeQuery("SELECT * FROM PLATTEN");
            while (rs.next()) {
                String name = rs.getString("ALBUM");
                System.out.println(name);
            }*/
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

}
package Objekte;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Stephan von Greiffenstern on 05.02.15.
 */
public class PlatteSL {
//Album, interpret, genre, jahr, kataloh
    private final SimpleStringProperty album;
    private final SimpleStringProperty interpret;
    private final SimpleStringProperty genre;
    private final SimpleIntegerProperty jahr;
    private final SimpleStringProperty katnr;
    private Platte platte;

    public PlatteSL(Platte p) {
        platte = p;
        album = new SimpleStringProperty(p.getName());
        interpret = new SimpleStringProperty(p.getInterpret());
        genre = new SimpleStringProperty(p.getGenre());
        jahr = new SimpleIntegerProperty(p.getYear());
        katnr = new SimpleStringProperty(p.getKatnr());
    }

    public String getAlbum() {
        return album.get();
    }

    public SimpleStringProperty albumProperty() {
        return album;
    }

    public String getInterpret() {
        return interpret.get();
    }

    public SimpleStringProperty interpretProperty() {
        return interpret;
    }

    public String getGenre() {
        return genre.get();
    }

    public SimpleStringProperty genreProperty() {
        return genre;
    }

    public int getJahr() {
        return jahr.get();
    }

    public SimpleIntegerProperty jahrProperty() {
        return jahr;
    }

    public String getKatnr() {
        return katnr.get();
    }

    public SimpleStringProperty katnrProperty() {
        return katnr;
    }

    public Platte getPlatte() {
        return platte;
    }
}

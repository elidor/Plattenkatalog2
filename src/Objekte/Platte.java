package Objekte;

/**
 * Created by Stephan von Greiffenstern on 04.02.15.
 * Das gibt die Datenbankverbindung zurück und nimmt es zum Speichern auch wieder an :)
 */
public class Platte {
    private int id = -1;
    private String name;
    private String interpret;
    private int year = 0;
    private String genre="";
    private String cond="";
    private String owner="";
    private String location="";
    private int purchase;
    private int forsell;
    private String songs="";
    private String katnr="";
    private String other="";

    public int getId() {
        return id;
    }

    public boolean hasId() {
        return !(id < 0) ? true : false;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String pName) {
        if (pName.length() > 50) {
            name = pName.substring(0, 50);
        } else {
            name = pName;
        }
    }

    public String getInterpret() {
        return interpret;
    }

    public void setInterpret(String pInterpret) {
        if (pInterpret.length() > 50) {
            interpret = pInterpret.substring(0, 50);
        } else {
            interpret = pInterpret;
        }
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;

        if (this.year < 100) {
            this.year += 1900;
        }
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String pGenre) {
        if (pGenre != null) {
            if (pGenre.length() > 15) {
                genre = pGenre.substring(0, 15);
            } else {
                genre = pGenre;
            }
        }
    }

    public String getCond() {
        return cond;
    }

    public void setCond(String pCond) {
        if (pCond != null) {
            if (pCond.length() > 100) {
                cond = pCond.substring(0, 100);
            } else {
                cond = pCond;
            }
        }
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String pOwner) {
        if (pOwner != null) {
            if (pOwner.length() > 35) {
                owner = pOwner.substring(0, 35);
            } else {
                owner = pOwner;
            }
        }
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String pLocation) {
        if (pLocation != null) {
            if (pLocation.length() > 50) {
                location = pLocation.substring(0, 50);
            } else {
                location = pLocation;
            }
        }
    }

    public int getPurchase() {
        return purchase;
    }

    public void setPurchase(int purchase) {
        this.purchase = purchase;
    }

    public int getForsell() {
        return forsell;
    }

    public boolean isForsell() {
        if (forsell > 0) return true;
        else return false;
    }

    public void setForsell(int forsell) {
        this.forsell = forsell;
    }

    public String getSongs() {
        return songs;
    }

    public void setSongs(String pSongs) {
        if (pSongs != null) {
            if (pSongs.length() > 1000) {
                songs = pSongs.substring(0, 1000);
            } else {
                songs = pSongs;
            }
        }
    }

    public String getKatnr() {
        return katnr;
    }

    public void setKatnr(String katnr) {
        this.katnr = katnr;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }
}

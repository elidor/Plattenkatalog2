package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.lang.reflect.Array;

/**
 * Created by Stephan von Greiffenstern on 05.02.15.
 */
public class Main extends Application{
    @Override
    public void start(Stage primaryStage) {
        try {
            final FXMLLoader loader = new FXMLLoader(getClass().getResource("GUI.fxml"));
            final Parent root = (Parent) loader.load();
            Scene scene = new Scene(root);
            primaryStage.setTitle("Plattenkatalog - by Stephan von Greiffenstern");
            primaryStage.setScene(scene);
            primaryStage.show();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void Main(String[] args) {
        launch(args);
    }
}

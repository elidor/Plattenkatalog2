package GUI;

import Database.Databaseconnection;
import Objekte.Platte;
import Objekte.PlatteSL;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Stephan von Greiffenstern on 05.02.15.
 */
public class Controller implements Initializable {

    @FXML
    private TextField search;
    @FXML
    private TextField album;
    @FXML
    private TextField interpret;
    @FXML
    private TextField jahr;
    @FXML
    private TextField genre;
    @FXML
    private TextField katnr;
    @FXML
    private TextField owner;
    @FXML
    private TextField price;
    @FXML
    private TextField sellprice;

    @FXML
    private TextArea titel;
    @FXML
    private TextArea location;
    @FXML
    private TextArea condition;
    @FXML
    private TextArea other;

    @FXML
    private TableView searchresults;
    @FXML
    private TableColumn cAlbum;
    @FXML
    private TableColumn cInterpret;
    @FXML
    private TableColumn cGenre;
    @FXML
    private TableColumn cJahr;
    @FXML
    private TableColumn cKatNr;

    private Databaseconnection dbc;
    private boolean newItem = false;


    @FXML
    private void handleButtonNew(ActionEvent e) {
        Platte p = new Platte();
        showItem(p);
        newItem = true;
    }

    @FXML
    private void handleKlickSearch(MouseEvent e) {
        if (e.getClickCount() > 2) {
            PlatteSL psl = (PlatteSL) searchresults.getSelectionModel().getSelectedItem();
            Platte p = psl.getPlatte();
            dbc.deleteEntry(p);
            search();
        } else {

            PlatteSL psl = (PlatteSL) searchresults.getSelectionModel().getSelectedItem();
            Platte p = psl.getPlatte();
            showItem(p);
        }
    }

    @FXML
    private void handleKeyPressedSearchResults(KeyEvent event) {
        PlatteSL psl = (PlatteSL) searchresults.getSelectionModel().getSelectedItem();
        Platte p = psl.getPlatte();
        showItem(p);
    }

    @FXML
    private void handleButtonSave(ActionEvent e) {
        Platte p = read();
        newItem = false;
        dbc.refEntry(p);
        search();
        //TODO: "'" rausfiltern und durch \' ersetzen oder ganz löschen
    }

    @FXML
    private void handleKeyPressedSearchField(KeyEvent event) {
        search();
    }

    private void search() {
        Platte[] p;
        p = dbc.search(search.getText());

        if (p != null) {
            PlatteSL[] tsl = new PlatteSL[p.length];
            for (int i = 0; i < p.length; i++) {
                tsl[i] = new PlatteSL(p[i]);
            }

            final ObservableList<PlatteSL> data = FXCollections.observableArrayList(tsl);

            searchresults.setItems(data);
        } else { //Liste leeren
            final ObservableList<PlatteSL> data = FXCollections.observableArrayList();

            searchresults.setItems(data);
        }
    }

    private void showItem(Platte p) {
        album.setText(p.getName());
        interpret.setText(p.getInterpret());
        jahr.setText("" + p.getYear());
        genre.setText(p.getGenre());
        katnr.setText(p.getKatnr());
        owner.setText(p.getOwner());
        titel.setText(p.getSongs());
        location.setText(p.getLocation());
        condition.setText(p.getCond());
        if (!p.getOther().equals("")) {
            other.setText(p.getOther());
        } else {
            other.setText("");
        }

        int i = p.getPurchase();
        if (i < 0) {
            price.setText("unbekannt");
        } else {
            price.setText(i + " €");
        }
        i = p.getForsell();
        if (i < 0) {
            sellprice.setText("unverkäuflich");
        } else {
            sellprice.setText(i + " €");
        }
    }

    private Platte read() {
        Platte p;
        if (newItem) {
            p = new Platte();
        } else {
            p = ((PlatteSL) searchresults.getSelectionModel().getSelectedItem()).getPlatte();
        }
        p.setName(album.getText());
        p.setInterpret(interpret.getText());
        p.setYear(Integer.parseInt(jahr.getText()));
        p.setGenre(genre.getText());
        p.setKatnr(katnr.getText());
        p.setOwner(owner.getText());
        p.setSongs(titel.getText());
        p.setLocation(location.getText());
        p.setCond(condition.getText());
        p.setOther(other.getText());

        String preis = price.getText().replace(" ", "").replace("€", "");
        if (isNumeric(preis)) {
            p.setPurchase(Integer.parseInt(preis));
        } else {
            p.setPurchase(-1);
        }
        preis = sellprice.getText().replace(" ", "").replace("€", "");
        if (isNumeric(preis)) {
            p.setForsell(Integer.parseInt(preis));
        } else {
            p.setForsell(-1);
        }
        return p;
    }

    private boolean isNumeric(String s) {
        char[] cA = s.toCharArray();
        for (char c : cA) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cAlbum.setCellValueFactory(new PropertyValueFactory<PlatteSL, String>("album"));
        cInterpret.setCellValueFactory(new PropertyValueFactory<PlatteSL, String>("interpret"));
        cGenre.setCellValueFactory(new PropertyValueFactory<PlatteSL, String>("genre"));
        cJahr.setCellValueFactory(new PropertyValueFactory<PlatteSL, Integer>("jahr"));
        cKatNr.setCellValueFactory(new PropertyValueFactory<PlatteSL, String>("katnr"));

        try {
            dbc = new Databaseconnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        search();
    }
}
